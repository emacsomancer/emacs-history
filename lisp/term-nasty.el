;;; term-nasty.el --- Damned Things from terminfo.el

;;; This text is no longer included in Emacs, because it was censored
;;; by the Communications Decency Act.  The law was promoted as a ban
;;; on pornography, but it bans far more than that.  This file did not
;;; contain pornography, but it was prohibited nonetheless.

;;; For information on US government censorship of the Internet, and
;;; what you can do to bring back freedom of the press, see the web
;;; site http://www.vtw.org/

;;; term-nasty.el ends here
