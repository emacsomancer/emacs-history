\initial {.}
\entry {.emacs file}{12, 13, 14, 15}
\initial {A}
\entry {add-hook substitute}{13}
\entry {attribute, attributing}{1}
\entry {attribution info field (sc-)}{7}
\entry {attribution list}{21}
\entry {attribution string}{5}
\entry {author info field (sc-)}{8}
\entry {author names}{25}
\entry {autoload}{12}
\initial {C}
\entry {citation}{3}
\entry {citation delimiter}{5}
\entry {citation info field (sc-)}{7}
\entry {citation interface specification}{11}
\entry {citation leader}{5}
\entry {citation separator}{5}
\entry {citation string}{4}
\entry {cite, citing}{1}
\initial {E}
\entry {electric references}{10}
\entry {emailname info field (sc-)}{8}
\entry {entries (Regi)}{26}
\initial {F}
\entry {filladapt}{3, 20}
\entry {filling paragraphs}{20}
\entry {firstname info field (sc-)}{8}
\entry {frames (Regi)}{26}
\entry {from-address info field (sc-)}{7}
\initial {G}
\entry {gin-mode}{3, 20}
\initial {H}
\entry {header rewrite functions}{9}
\entry {header rewrite functions, built-in}{9}
\entry {Hyperbole}{16}
\initial {I}
\entry {info alist}{2}
\entry {Info Alist}{6}
\entry {information extracted from mail fields}{6}
\entry {information keys}{6}
\entry {initials info field (sc-)}{8}
\initial {K}
\entry {keymap prefix}{18}
\initial {L}
\entry {lastname info field (sc-)}{8}
\initial {M}
\entry {mail-citation-hook}{14}
\entry {mailing list address}{39}
\entry {mark}{12}
\entry {middlename-1 info field (sc-)}{8}
\entry {modeline}{3, 18}
\entry {MUA}{1}
\initial {N}
\entry {nested citations}{3}
\entry {non-nested citations}{4}
\entry {NUA}{1}
\entry {nuking mail headers}{2}
\initial {O}
\entry {overloading}{13, 15}
\initial {P}
\entry {point}{12}
\initial {R}
\entry {reciting}{29}
\entry {reference header}{2}
\entry {reference headers}{8}
\entry {Regi}{26}
\entry {regi.el file}{38}
\entry {reply-address info field (sc-)}{7}
\entry {reporter.el file}{38}
\initial {S}
\entry {sc-attribution info field}{7}
\entry {sc-author info field}{8}
\entry {sc-citation info field}{7}
\entry {sc-elec.el from version 2}{38}
\entry {sc-emailname info field}{8}
\entry {sc-firstname info field}{8}
\entry {sc-from-address info field}{7}
\entry {sc-initials info field}{8}
\entry {sc-lastname info field}{8}
\entry {sc-middlename-1 info field}{8}
\entry {sc-oloads.el}{15}
\entry {sc-reply-address info field}{7}
\entry {sc-sender-address info field}{7}
\entry {sc-unsupp.el file}{13, 38}
\entry {sc.el from version 2}{38}
\entry {sender-address info field (sc-)}{7}
\entry {sendmail.el}{36}
\entry {sendmail.el file}{13}
\entry {setq as a substitute for add-hook}{13}
\entry {supercite mailing list address}{39}
\entry {supercite.el file}{38}
\initial {T}
\entry {toggling variables}{32}
\initial {U}
\entry {unciting}{29}
\entry {undo boundary}{18}
\initial {Y}
\entry {yank}{1}
