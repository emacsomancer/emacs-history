\initial {C}
\entry {Comparing files and buffers}{1}
\initial {E}
\entry {ediff}{2}
\entry {ediff-after-flag-eol}{20}
\entry {ediff-after-flag-mol}{20}
\entry {ediff-after-setup-control-frame-hook}{14}
\entry {ediff-after-setup-windows-hook}{13}
\entry {ediff-auto-refine}{22}
\entry {ediff-auto-refine-limit}{23}
\entry {ediff-before-flag-bol}{20}
\entry {ediff-before-flag-mol}{20}
\entry {ediff-before-setup-control-frame-hook}{14}
\entry {ediff-before-setup-windows-hook}{13}
\entry {ediff-brief-help-message-function}{30}
\entry {ediff-buffers}{2}
\entry {ediff-buffers3}{2}
\entry {ediff-cleanup-hook}{14}
\entry {ediff-combination-pattern}{25}
\entry {ediff-control-buffer}{15}
\entry {ediff-control-frame-parameters}{16}
\entry {ediff-control-frame-position-function}{16}
\entry {ediff-control-frame-upward-shift}{17}
\entry {ediff-current-diff-face-A}{20}
\entry {ediff-current-diff-face-B}{20}
\entry {ediff-current-diff-face-C}{20}
\entry {ediff-custom-diff-options}{24}
\entry {ediff-custom-diff-program}{24}
\entry {ediff-default-variant}{25}
\entry {ediff-diff-options}{24}
\entry {ediff-diff-program}{23}
\entry {ediff-diff3-options}{24}
\entry {ediff-diff3-program}{23}
\entry {ediff-directories}{2}
\entry {ediff-directories3}{2}
\entry {ediff-directory-revisions}{2}
\entry {ediff-display-help-hook}{14}
\entry {ediff-even-diff-face-A}{20}
\entry {ediff-even-diff-face-B}{20}
\entry {ediff-even-diff-face-C}{20}
\entry {ediff-files}{2}
\entry {ediff-files3}{2}
\entry {ediff-fine-diff-face-A}{20}
\entry {ediff-fine-diff-face-B}{20}
\entry {ediff-fine-diff-face-C}{20}
\entry {ediff-force-faces}{20}
\entry {ediff-forward-word}{23}
\entry {ediff-forward-word-function}{23}
\entry {\code {ediff-grab-mouse}}{30}
\entry {ediff-help-message}{15}
\entry {ediff-highlight-all-diffs}{20}
\entry {ediff-ignore-similar-regions}{19}
\entry {ediff-janitor}{14}
\entry {ediff-job-name}{30}
\entry {\code {ediff-keep-variants}}{29}
\entry {ediff-keymap-setup-hook}{13}
\entry {ediff-load-hook}{13}
\entry {ediff-long-help-message-function}{30}
\entry {ediff-make-frame-position}{16}
\entry {ediff-make-wide-display-function}{29}
\entry {ediff-merge}{3}
\entry {ediff-merge-buffers}{3}
\entry {ediff-merge-buffers-with-ancestor}{3}
\entry {ediff-merge-directories}{3}
\entry {ediff-merge-directories-with-ancestor}{3}
\entry {ediff-merge-directory-revisions}{2}
\entry {ediff-merge-directory-revisions-with-ancestor}{2}
\entry {ediff-merge-files}{3}
\entry {ediff-merge-files-with-ancestor}{3}
\entry {ediff-merge-revisions}{3}
\entry {ediff-merge-revisions-with-ancestor}{3}
\entry {ediff-merge-split-window-function}{29}
\entry {ediff-merge-window-share}{26}
\entry {ediff-merge-with-ancestor}{3}
\entry {ediff-mode-hook}{15}
\entry {ediff-narrow-control-frame-leftward-shift}{16}
\entry {ediff-no-emacs-help-in-control-buffer}{29}
\entry {ediff-odd-diff-face-A}{20}
\entry {ediff-odd-diff-face-B}{20}
\entry {ediff-odd-diff-face-C}{20}
\entry {ediff-patch-buffer}{3}
\entry {ediff-patch-default-directory}{24}
\entry {ediff-patch-file}{3}
\entry {ediff-patch-options}{24}
\entry {ediff-patch-program}{23}
\entry {ediff-prefer-iconified-control-frame}{17}
\entry {ediff-prepare-buffer-hook}{14, 28}
\entry {ediff-profile}{8}
\entry {ediff-quit-hook}{13}
\entry {ediff-quit-widened}{22}
\entry {ediff-regions-linewise}{3}
\entry {ediff-regions-wordwise}{3}
\entry {ediff-registry-setup-hook}{15}
\entry {ediff-revert-buffers-then-recompute-diffs}{8}
\entry {ediff-revision}{3}
\entry {ediff-save-buffer}{24}
\entry {ediff-select-hook}{14}
\entry {ediff-session-group-setup-hook}{15}
\entry {ediff-setup}{30}
\entry {ediff-setup-windows}{17}
\entry {ediff-setup-windows-multiframe}{17}
\entry {ediff-setup-windows-plain}{17}
\entry {ediff-show-clashes-only}{27}
\entry {ediff-show-registry}{8}
\entry {ediff-split-window-function}{28}
\entry {ediff-start-narrowed}{22}
\entry {ediff-startup-hook}{14, 15, 30}
\entry {ediff-suspend-hook}{13}
\entry {ediff-toggle-multiframe}{8, 17}
\entry {ediff-toggle-read-only-function}{29}
\entry {ediff-unselect-hook}{14}
\entry {\code {ediff-use-last-dir}}{4}
\entry {ediff-use-last-dir}{29}
\entry {ediff-use-long-help-message}{15}
\entry {ediff-version-control-package}{27}
\entry {ediff-wide-control-frame-rightward-shift}{17}
\entry {ediff-window-setup-function}{17}
\entry {ediff-windows-linewise}{2}
\entry {ediff-windows-wordwise}{2}
\entry {ediff-word-1}{23}
\entry {ediff-word-2}{23}
\entry {ediff-word-3}{23}
\entry {ediff-word-4}{23}
\entry {ediff-word-mode}{30}
\entry {ediff3}{2}
\entry {edir-merge-revisions}{2}
\entry {edir-merge-revisions-with-ancestor}{2}
\entry {edir-revisions}{2}
\entry {edirs}{2}
\entry {edirs-merge}{3}
\entry {edirs-merge-with-ancestor}{3}
\entry {edirs3}{2}
\entry {epatch}{3}
\entry {eregistry}{8}
\initial {F}
\entry {Finding differences}{1}
\initial {G}
\entry {\file {generic-sc.el}}{28}
\initial {M}
\entry {Merging files and buffers}{1}
\entry {\file {mode-line.el}}{28}
\entry {Multi-file patches}{10}
\initial {P}
\entry {Patching files and buffers}{1}
\entry {\file {pcl-cvs.el}}{28}
\initial {R}
\entry {\file {rcs.el}}{28}
\initial {U}
\entry {\file {uniquify.el}}{28}
\initial {V}
\entry {\file {vc.el}}{28}
