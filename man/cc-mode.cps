\initial {-}
\entry {-block-intro syntactic symbols}{32}
\entry {-close syntactic symbols}{32}
\entry {-cont syntactic symbols}{32}
\entry {-intro syntactic symbols}{32}
\entry {-open syntactic symbols}{32}
\initial {.}
\entry {\file {.emacs} file}{3}
\initial {A}
\entry {Adding Styles}{26}
\entry {Advanced Customizations}{27}
\entry {announcement mailing list}{49}
\entry {Auto-newline insertion}{12}
\initial {B}
\entry {basic-offset (c-)}{20}
\entry {beta testers mailing list}{49}
\entry {block-close syntactic symbol}{12}
\entry {block-open syntactic symbol}{12}
\entry {BOCM}{1}
\entry {brace lists}{36}
\entry {brace-list-close syntactic symbol}{12}
\entry {brace-list-entry syntactic symbol}{12}
\entry {brace-list-intro syntactic symbol}{12}
\entry {brace-list-open syntactic symbol}{12}
\entry {BSD style}{24}
\entry {Built-in Styles}{24}
\entry {byte compile}{3}
\initial {C}
\entry {c-basic-offset}{20}
\entry {c-hanging- functions}{18}
\entry {c-set-offset}{20}
\entry {\file {cc-compat.el} file}{1}
\entry {\file {cc-lobotomy.el} file}{40}
\entry {CC-MODE style}{25}
\entry {\file {cc-mode-18.el} file}{3}
\entry {class-close syntactic symbol}{12}
\entry {class-open syntactic symbol}{12}
\entry {clean-ups}{14}
\entry {Clean-ups}{15}
\entry {comment only line}{7}
\entry {comment-only line}{14}
\entry {Custom Brace and Colon Hanging}{29}
\entry {custom indentation function}{12}
\entry {custom indentation functions}{27}
\entry {Custom Indentation Functions}{27}
\entry {customizing brace hanging}{29}
\entry {customizing colon hanging}{30}
\entry {Customizing Indentation}{20}
\entry {customizing semi-colons and commas}{30}
\entry {Customizing Semi-colons and Commas}{30, 31}
\initial {D}
\entry {defun-close syntactic symbol}{12}
\entry {defun-open syntactic symbol}{12}
\initial {E}
\entry {electric characters}{11}
\entry {electric commands}{12}
\entry {Ellemtel style}{25}
\initial {F}
\entry {File Styles}{26}
\entry {Frequently Asked Questions}{42}
\initial {G}
\entry {Getting Connected}{3}
\entry {Getting the latest \code {cc-mode} release}{44}
\entry {GNU style}{24}
\initial {H}
\entry {Hanging Braces}{12}
\entry {Hanging Colons}{13}
\entry {Hanging Semi-colons and commas}{14}
\entry {hooks}{23}
\entry {Hungry-deletion of whitespace}{16}
\initial {I}
\entry {in-class inline methods}{33}
\entry {Indentation Calculation}{8}
\entry {Indentation Commands}{18}
\entry {inline-close}{48}
\entry {inline-close syntactic symbol}{12}
\entry {inline-open syntactic symbol}{12}
\entry {Interactive Customization}{21}
\entry {Introduction}{1}
\initial {J}
\entry {Java style}{25}
\entry {java-mode}{25}
\initial {K}
\entry {K&R style}{24}
\initial {L}
\entry {Limitations and Known Bugs}{48}
\entry {literal}{12, 15, 17, 18}
\entry {local variables}{26}
\initial {M}
\entry {Mailing Lists and Submitting Bug Reports}{49}
\entry {Minor Modes}{11}
\entry {modifier syntactic symbol}{38}
\initial {N}
\entry {New Indentation Engine}{6}
\initial {O}
\entry {Other electric commands}{14}
\initial {P}
\entry {Performance Issues}{40}
\entry {Permanent Indentation}{23}
\initial {R}
\entry {relative buffer position}{6}
\entry {reporter.el}{47}
\entry {Requirements}{47}
\initial {S}
\entry {Sample \file {.emacs} file}{45}
\entry {set-offset (c-)}{20}
\entry {statement-case-open syntactic symbol}{12}
\entry {stream-op syntactic symbol}{28}
\entry {Stroustrup style}{24}
\entry {Styles}{24}
\entry {substatement}{7}
\entry {substatement block}{7}
\entry {substatement-open syntactic symbol}{12}
\entry {Syntactic Analysis}{6}
\entry {syntactic component}{6}
\entry {syntactic component list}{6}
\entry {syntactic symbol}{6}
\entry {Syntactic Symbols}{32}
\entry {syntactic whitespace}{12, 38}
\initial {T}
\entry {TAB}{10}
\initial {W}
\entry {Whitesmith style}{24}
