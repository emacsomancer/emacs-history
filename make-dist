#!/bin/sh

#### make-dist: create an Emacs distribution tar file from the current
#### source tree.  This basically creates a duplicate directory
#### structure, and then hard links into it only those files that should
#### be distributed.  This means that if you add a file with an odd name,
#### you should make sure that this script will include it.

# Copyright (C) 1995 Free Software Foundation, Inc.
#
# This file is part of GNU Emacs.
#
# GNU Emacs is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# GNU Emacs is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GNU Emacs; see the file COPYING.  If not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

progname="$0"

### Exit if a command fails.
### set -e

### Print out each line we read, for debugging's sake.
### set -v

clean_up=no
make_tar=no
newer=""

while [ $# -gt 0 ]; do
  case "$1" in
    ## This option tells make-dist to delete the staging directory
    ## when done.  It is useless to use this unless you make a tar file.
    "--clean-up" )
      clean_up=yes
    ;;
    ## This option tells make-dist to make a tar file.
    "--tar" )
      make_tar=yes
    ;;
    ## This option tells make-dist to make the distribution normally, then
    ## remove all files older than the given timestamp file.  This is useful
    ## for creating incremental or patch distributions.
    "--newer")
      newer="$2"
      new_extension=".new"
      shift
    ;;
    ## This option tells make-dist to use `compress' instead of gzip.
    ## Normally, make-dist uses gzip whenever it is present.
    "--compress")
      default_gzip="compress"
    ;;
    * )
      echo "${progname}: Unrecognized argument: $1" >&2
      exit 1
    ;;
  esac
  shift
done

### Make sure we're running in the right place.
if [ ! -d src -o ! -f src/lisp.h -o ! -d lisp -o ! -f lisp/version.el ]; then
  echo "${progname}: Can't find \`src/lisp.h' and \`lisp/version.el'." >&2
  echo "${progname} must be run in the top directory of the Emacs" >&2
  echo "distribution tree.  cd to that directory and try again." >&2
  exit 1
fi

### Find out which version of Emacs this is.
shortversion=`grep 'defconst[	 ]*emacs-version' lisp/version.el \
	 | sed -e 's/^.*"\([0-9][0-9]*\.[0-9][0-9]*\).*$/\1/'`
version=`grep 'defconst[	 ]*emacs-version' lisp/version.el \
	 | sed -e 's/^[^"]*"\([^"]*\)".*$/\1/'`
if [ ! "${version}" ]; then
  echo "${progname}: can't find current Emacs version in \`./lisp/version.el'" >&2
  exit 1
fi

echo Version numbers are $version and $shortversion

if grep -s "GNU Emacs version ${shortversion}" ./man/emacs.texi > /dev/null; then
  true
else
  echo "You must update the version number in \`./man/emacs.texi'"
  sleep 5
fi

### Make sure we don't already have a directory  emacs-${version}.

emacsname="emacs-${version}${new_extension}"

if [ -d ${emacsname} ]
then
  echo Directory "${emacsname}" already exists >&2
  exit 1
fi

### Make sure the subdirectory is available.
tempparent="make-dist.tmp.$$"
if [ -d ${tempparent} ]; then
  echo "${progname}: staging directory \`${tempparent}' already exists.
Perhaps a previous invocation of \`${progname}' failed to clean up after
itself.  Check that directories whose names are of the form
\`make-dist.tmp.NNNNN' don't contain any important information, remove
them, and try again." >&2
  exit 1
fi

### Check for .elc files with no corresponding .el file.
ls -1 lisp/*.el | sed 's/\.el$/.elc/' > /tmp/el
ls -1 lisp/*.elc > /tmp/elc
bogosities="`comm -13 /tmp/el /tmp/elc`"
if [ "${bogosities}" != "" ]; then
  echo "The following .elc files have no corresponding .el files:"
  echo "${bogosities}"
fi
rm -f /tmp/el /tmp/elc

### Check for .el files that would overflow the 14-char limit if compiled.
long=`find lisp -name '[a-zA-Z0-9]??????????*.el' -print`
if [ "$long" != "" ]; then
  echo "The following .el file names are too long:"
  echo "$long"
fi

### Make sure configure is newer than configure.in.
if [ "x`ls -t configure configure.in | head -1`" != "xconfigure" ]; then
  echo "\`./configure.in' is newer than \`./configure'" >&2
  echo "Running autoconf" >&2
  autoconf || { x=$?; echo Autoconf FAILED! >&2; exit $x; }
fi

echo "Updating Info files"

(cd man; make info)

echo "Updating finder-inf.el"

(cd lisp; ../src/emacs -batch -l finder -f finder-compile-keywords-make-dist)

echo "Recompiling Lisp files"

src/emacs -batch -f batch-byte-recompile-directory lisp

echo "Updating autoloads"

src/emacs -batch -f batch-update-autoloads lisp

echo "Making lisp/MANIFEST"

(cd lisp; head -1 [!=]*.el | grep '^;' | sed -e 's/;;; //' > MANIFEST)

echo "Creating staging directory: \`${tempparent}'"

mkdir ${tempparent}
tempdir="${tempparent}/${emacsname}"

### This trap ensures that the staging directory will be cleaned up even
### when the script is interrupted in mid-career.
if [ "${clean_up}" = yes ]; then
  trap "echo 'Interrupted...cleaning up the staging directory'; rm -rf ${tempparent}; exit 1" 1 2 15
fi

echo "Creating top directory: \`${tempdir}'"
mkdir ${tempdir}

### We copy in the top-level files before creating the subdirectories in
### hopes that this will make the top-level files appear first in the
### tar file; this means that people can start reading the INSTALL and
### README while the rest of the tar file is still unpacking.  Whoopee.
echo "Making links to top-level files"
ln GETTING.GNU.SOFTWARE INSTALL PROBLEMS README BUGS move-if-change ${tempdir}
ln ChangeLog Makefile.in configure configure.in ${tempdir}
ln config.bat make-dist update-subdirs vpath.sed ${tempdir}
### Copy these files; they're cross-filesystem symlinks.
cp mkinstalldirs ${tempdir}
cp config.sub ${tempdir}
cp config.guess ${tempdir}
cp install.sh ${tempdir}

echo "Updating version number in README"
(cd ${tempdir}
 awk \
   '$1 " " $2 " " $3 " " $4 " " $5 == "This directory tree holds version" { $6 = version; print $0 }
    $1 " " $2 " " $3 " " $4 " " $5 != "This directory tree holds version"' \
   version=${version} README > tmp.README
 mv tmp.README README)


echo "Creating subdirectories"
for subdir in lisp lisp/term site-lisp \
	      src src/m src/s src/bitmaps lib-src oldXMenu lwlib \
	      nt nt/inc nt/inc/sys nt/inc/arpa nt/inc/netinet \
	      etc etc/e lock cpp info man msdos vms; do
  mkdir ${tempdir}/${subdir}
done

echo "Making links to \`lisp'"
### Don't distribute TAGS, =*.el files, site-init.el, site-load.el, or default.el.
(cd lisp
 ln [a-zA-Z]*.el ../${tempdir}/lisp
 ln [a-zA-Z]*.elc ../${tempdir}/lisp
 ln [a-zA-Z]*.dat ../${tempdir}/lisp
 ## simula.el doesn't keep abbreviations in simula.defns any more.
 ## ln [a-zA-Z]*.defns ../${tempdir}/lisp
 ln ChangeLog Makefile makefile.nt ChangeLog.? README ../${tempdir}/lisp
 cd ../${tempdir}/lisp
 rm -f TAGS =*
 rm -f subdirs.el
 rm -f site-init site-init.el site-init.elc
 rm -f site-load site-load.el site-load.elc
 rm -f site-start site-start.el site-start.elc
 rm -f default default.el default.elc)

#echo "Making links to \`lisp/calc-2.02'"
#### Don't distribute =*.el files, TAGS or backups.
#(cd lisp/calc-2.02
# ln [a-zA-Z]*.el ../../${tempdir}/lisp/calc-2.02
# ln [a-zA-Z]*.elc ../../${tempdir}/lisp/calc-2.02
# ln calc.info* calc.texinfo calc-refcard.* ../../${tempdir}/lisp/calc-2.02
# ln INSTALL Makefile README README.prev ../../${tempdir}/lisp/calc-2.02
# cd ../../${tempdir}/lisp/calc-2.02
# rm -f *~ TAGS)

echo "Making links to \`lisp/term'"
### Don't distribute =*.el files or TAGS.
(cd lisp/term
 ln [a-zA-Z]*.el ../../${tempdir}/lisp/term
 ln [a-zA-Z]*.elc ../../${tempdir}/lisp/term
 ln README ../../${tempdir}/lisp/term
 rm -f =* TAGS)

echo "Making links to \`src'"
### Don't distribute =*.[ch] files, or the configured versions of
### config.in, paths.in, or Makefile.in, or TAGS.
(cd src
 echo "  (If we can't link gmalloc.c, that's okay.)"
 ln [a-zA-Z]*.c ../${tempdir}/src
 ## Might be a symlink to a file on another filesystem.
 test -f ../${tempdir}/src/gmalloc.c || cp gmalloc.c ../${tempdir}/src
 ln [a-zA-Z]*.h ../${tempdir}/src
 ln [a-zA-Z]*.s ../${tempdir}/src
 ln README Makefile.in ChangeLog ChangeLog.? config.in paths.in \
    ../${tempdir}/src
 ln makefile.nt ../${tempdir}/src
 ln .gdbinit .dbxinit ../${tempdir}/src
 ln *.opt vms-pp.trans ../${tempdir}/src
 cd ../${tempdir}/src
 rm -f config.h paths.h Makefile Makefile.c
 rm -f =* TAGS)

echo "Making links to \`src/bitmaps'"
(cd src/bitmaps
 ln README *.xbm ../../${tempdir}/src/bitmaps)

echo "Making links to \`src/m'"
(cd src/m
 # We call files for miscellaneous input (to linker etc) .inp.
 ln README [a-zA-Z0-9]*.h *.inp ../../${tempdir}/src/m)

echo "Making links to \`src/s'"
(cd src/s
 ln README [a-zA-Z0-9]*.h ../../${tempdir}/src/s)

echo "Making links to \`lib-src'"
(cd lib-src
 ln [a-zA-Z]*.[chy] ../${tempdir}/lib-src
 ln ChangeLog Makefile.in README testfile vcdiff ../${tempdir}/lib-src
 ln emacs.csh rcs2log rcs-checkin makefile.nt ../${tempdir}/lib-src
 cd ../${tempdir}/lib-src
 rm -f getdate.tab.c y.tab.c y.tab.h Makefile.c
 rm -f =* TAGS)

echo "Making links to \`nt'"
(cd nt
 ln emacs.ico emacs.rc config.nt [a-z]*.in [a-z]*.c ../${tempdir}/nt
 ln [a-z]*.bat [a-z]*.h makefile.def makefile.nt ../${tempdir}/nt
 ln TODO ChangeLog INSTALL README ../${tempdir}/nt)

echo "Making links to \`nt/inc'"
(cd nt/inc
 ln [a-z]*.h ../../${tempdir}/nt/inc)

echo "Making links to \`nt/inc/sys'"
(cd nt/inc/sys
 ln [a-z]*.h ../../../${tempdir}/nt/inc/sys)

echo "Making links to \`nt/inc/arpa'"
(cd nt/inc/arpa
 ln [a-z]*.h ../../../${tempdir}/nt/inc/arpa)

echo "Making links to \`nt/inc/netinet'"
(cd nt/inc/netinet
 ln [a-z]*.h ../../../${tempdir}/nt/inc/netinet)

echo "Making links to \`msdos'"
(cd msdos
 ln  ChangeLog emacs.ico emacs.pif ../${tempdir}/msdos
 ln is_exec.c sigaction.c mainmake mainmake.v2 sed*.inp ../${tempdir}/msdos
 cd ../${tempdir}/msdos
 rm -f =*)

echo "Making links to \`oldXMenu'"
(cd oldXMenu
 ln *.c *.h *.in ../${tempdir}/oldXMenu
 ln README Imakefile ChangeLog ../${tempdir}/oldXMenu
 ln compile.com descrip.mms ../${tempdir}/oldXMenu)

echo "Making links to \`lwlib'"
(cd lwlib
 ln *.c *.h *.in ../${tempdir}/lwlib
 ln README Imakefile ChangeLog ../${tempdir}/lwlib
 cd ../${tempdir}/lwlib
 rm -f lwlib-Xol*)

echo "Making links to \`etc'"
### Don't distribute = files, TAGS, DOC files, backups, autosaves, or
### tex litter.
(cd etc
 ln `ls -d * | grep -v 'RCS' | grep -v 'Old' | grep -v '^e$'` ../${tempdir}/etc
 cd ../${tempdir}/etc
 rm -f DOC* *~ \#*\# *.dvi *.log *.orig *.rej *,v =* core
 rm -f TAGS)

echo "Making links to \`etc/e'"
(cd etc/e
 ln `ls -d * | grep -v 'RCS'` ../../${tempdir}/etc/e
 cd ../../${tempdir}/etc/e
 rm -f *~ \#*\# *,v =* core)

echo "Making links to \`cpp'"
(cd cpp
 ln cccp.c cexp.y Makefile README ../${tempdir}/cpp)

echo "Making links to \`info'"
# Don't distribute backups or autosaves.
(cd info
 ln [a-zA-Z]* ../${tempdir}/info
 cd ../${tempdir}/info
 # Avoid an error when expanding the wildcards later.
 ln emacs dummy~ ; ln emacs \#dummy\#
 rm -f *~ \#*\# core)

echo "Making links to \`man'"
(cd man
 ln *.texi *.aux *.cps *.fns *.kys *.vrs ../${tempdir}/man
 test -f README && ln README ../${tempdir}/man
 test -f Makefile.in && ln Makefile.in ../${tempdir}/man
 ln ChangeLog split-man ../${tempdir}/man
 cp texinfo.tex ../${tempdir}/man
 cd ../${tempdir}/man
 rm -f \#*\# =* *~ core emacs-index* *.Z *.z xmail
 rm -f emacs.?? termcap.?? gdb.?? *.log *.toc *.dvi *.oaux)

echo "Making links to \`vms'"
(cd vms
 ln [0-9a-zA-Z]* ../${tempdir}/vms
 cd ../${tempdir}/vms
 rm -f *~)

### It would be nice if they could all be symlinks to etc's copy, but
### you're not supposed to have any symlinks in distribution tar files.
echo "Making sure copying notices are all copies of \`etc/COPYING'"
rm -f ${tempdir}/etc/COPYING
cp etc/COPYING ${tempdir}/etc/COPYING
for subdir in lisp src lib-src info msdos; do
  if [ -f ${tempdir}/${subdir}/COPYING ]; then
    rm ${tempdir}/${subdir}/COPYING
  fi
  cp etc/COPYING ${tempdir}/${subdir}
done

#### Make sure that there aren't any hard links between files in the
#### distribution; people with afs can't deal with that.  Okay,
#### actually we just re-copy anything with a link count greater
#### than two.  (Yes, strictly greater than 2 is correct; since we
#### created these files by linking them in from the original tree,
#### they'll have exactly two links normally.)
####
#### Commented out since it's not strictly necessary; it should suffice
#### to just break the link on alloca.c.
#echo "Breaking intra-tree links."
#find ${tempdir} ! -type d -links +2 \
#  -exec cp -p {} $$ \; -exec rm -f {} \; -exec mv $$ {} \;
rm -f $tempdir/lib-src/alloca.c
cp $tempdir/src/alloca.c $tempdir/lib-src/alloca.c

if [ "${newer}" ]; then
  echo "Removing files older than $newer"
  ## We remove .elc files unconditionally, on the theory that anyone picking
  ## up an incremental distribution already has a running Emacs to byte-compile
  ## them with.
  find ${tempparent} \( -name '*.elc' -o ! -newer ${newer} \) -exec rm -f {} \;
fi

if [ "${make_tar}" = yes ]; then
  if [ "${default_gzip}" = "" ]; then
    echo "Looking for gzip"
    temppath=`echo $PATH | sed 's/^:/.:/
				s/::/:.:/g
				s/:$/:./
				s/:/ /g'`
    default_gzip=`(
      for dir in ${temppath}; do
	if [ -f ${dir}/gzip ]; then echo 'gzip --best'; exit 0; fi
      done
      echo compress
    )`
  fi
  case "${default_gzip}" in
    compress* ) gzip_extension=.Z ;;
    * )         gzip_extension=.gz ;;
  esac
  echo "Creating tar file"
  (cd ${tempparent} ; tar cvf - ${emacsname} ) \
    | ${default_gzip} \
    > ${emacsname}.tar${gzip_extension}
fi

if [ "${clean_up}" = yes ]; then
  echo "Cleaning up the staging directory"
  rm -rf ${tempparent}
else
  (cd ${tempparent}; mv ${emacsname} ..)
  rm -rf ${tempparent}
fi

### make-dist ends here
